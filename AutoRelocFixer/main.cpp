#include <windows.h>
#include <commctrl.h>
#include <stdio.h>
#include <vector>
#include "resource.h"
#include "TitanEngine\TitanEngine.h"

static HINSTANCE hInst;

struct SectionInfo
{
    unsigned int VirtualAddress;
    unsigned int RawEnd;
    char name[10];
};

struct RelocInfo
{
    unsigned int VirtualAddress;
    unsigned int Size;
    char name[10];
};

static std::vector<RelocInfo> relocList;
static wchar_t szSelectedFile[MAX_PATH]=L"";

static unsigned int GetRelocSize(ULONG_PTR FileMapVA, ULONG_PTR VirtualAddress, ULONG_PTR RawEnd)
{
    //Original tool "GetRelocSize" by Killboy/SND
    ULONG_PTR RelocDirAddr=VirtualAddress;
    unsigned int RelocSize=0;
    IMAGE_RELOCATION RelocDir;
    do
    {
        ULONG_PTR RawAddress=ConvertVAtoFileOffset(FileMapVA, RelocDirAddr, false);
        if(!RawAddress || RawAddress>=RawEnd) //invalid
            return 0;
        memcpy(&RelocDir, (unsigned char*)FileMapVA+RawAddress, sizeof(IMAGE_RELOCATION));
        if(!RelocDir.SymbolTableIndex)
            break;
        if(RawAddress+RelocDir.SymbolTableIndex>=RawEnd) //invalid
            return 0;
        RelocSize+=RelocDir.SymbolTableIndex;
        RelocDirAddr+=RelocDir.SymbolTableIndex;
    }
    while(RelocDir.VirtualAddress);
    return RelocSize;
}

static void LoadFile(HWND hwndDlg, const wchar_t* szFileName, bool silent)
{
    EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_FIX), false);
    wcscpy_s(szSelectedFile, szFileName);
    HANDLE FileHandle;
    DWORD LoadedSize;
    HANDLE FileMap;
    ULONG_PTR FileMapVA;
    if(StaticFileLoadW((wchar_t*)szFileName, UE_ACCESS_READ, false, &FileHandle, &LoadedSize, &FileMap, &FileMapVA))
    {
        int NumberOfSections=GetPE32DataFromMappedFile(FileMapVA, 0, UE_SECTIONNUMBER);
        if(NumberOfSections)
        {
            relocList.clear(); //clear list
            SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_RESETCONTENT, 0, 0);
            std::vector<SectionInfo> sectionList;
            for(int i=0; i<NumberOfSections; i++)
            {
                SectionInfo sectionInfo;
                sectionInfo.VirtualAddress=GetPE32DataFromMappedFile(FileMapVA, i, UE_SECTIONVIRTUALOFFSET);
                strcpy_s(sectionInfo.name, (const char*)GetPE32DataFromMappedFile(FileMapVA, i, UE_SECTIONNAME));
                unsigned int RawSize=GetPE32DataFromMappedFile(FileMapVA, i, UE_SECTIONRAWSIZE);
                unsigned int RawAddress=GetPE32DataFromMappedFile(FileMapVA, i, UE_SECTIONRAWOFFSET);
                if(!RawSize || !RawAddress || !sectionInfo.VirtualAddress)
                    continue;
                sectionInfo.RawEnd=RawAddress+RawSize;
                sectionList.push_back(sectionInfo);
            }
            ULONG_PTR ImageBase=GetPE32DataFromMappedFile(FileMapVA, 0, UE_IMAGEBASE);
            unsigned int RelocationTableAddress=GetPE32DataFromMappedFile(FileMapVA, 0, UE_RELOCATIONTABLEADDRESS);
            for(unsigned int i=0; i<sectionList.size(); i++)
            {
                SectionInfo sectionInfo=sectionList.at(i);
                unsigned int relocSize=GetRelocSize(FileMapVA, sectionInfo.VirtualAddress+ImageBase, sectionInfo.RawEnd);
                if(relocSize)
                {
                    RelocInfo relocInfo;
                    strcpy_s(relocInfo.name, sectionInfo.name);
                    relocInfo.Size=relocSize;
                    relocInfo.VirtualAddress=sectionInfo.VirtualAddress;
                    relocList.push_back(relocInfo);
                    char name[20]="";
                    if(relocInfo.VirtualAddress==RelocationTableAddress)
                        sprintf_s(name, "->\"%s\"", relocInfo.name);
                    else
                        sprintf_s(name, "  \"%s\"", relocInfo.name);
                    SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_ADDSTRING, 0, (LPARAM)name);
                }
            }
            char msg[256]="";
            sprintf_s(msg, "File analysis complete, found %u possible relocation section(s)!", relocList.size());
            if(relocList.size())
            {
                EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_FIX), true);
                int selectedIndex=0;
                SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_SETCURSEL, 0, 0);
                char info[256]="";
                sprintf_s(info, " Relocation Directory RVA: %.8X\r\nRelocation Directory Size: %.8X", relocList.at(selectedIndex).VirtualAddress, relocList.at(selectedIndex).Size);
                SetDlgItemTextA(hwndDlg, IDC_EDT_INFO, info);
            }
            if(!silent)
                MessageBoxA(hwndDlg, msg, "Done", MB_ICONINFORMATION);
        }
        else if(!silent)
            MessageBoxA(hwndDlg, "The file has no sections...", "Error", MB_ICONERROR);
        StaticFileUnloadW((wchar_t*)szFileName, false, FileHandle, LoadedSize, FileMap, FileMapVA);
    }
    else if(!silent)
        MessageBoxA(hwndDlg, "StaticFileLoad failed...", "Error", MB_ICONERROR);
}

static BOOL CALLBACK DlgMain(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch(uMsg)
    {
    case WM_INITDIALOG:
    {
        SendMessageA(hwndDlg, WM_SETICON, ICON_BIG, (LPARAM)LoadIconA(hInst, MAKEINTRESOURCEA(IDI_ICON1)));
        EnableWindow(GetDlgItem(hwndDlg, IDC_BTN_FIX), false);
        int argc;
        LPWSTR* argv=CommandLineToArgvW(GetCommandLineW(), &argc);
        if(argc>1) //command line argument
        {
            if(GetFileAttributesW(argv[1])==FILE_ATTRIBUTE_DIRECTORY)
                MessageBoxA(hwndDlg, "Directories are not supported.", "Error", MB_ICONERROR);
            else
            {
                __try
                {
                    LoadFile(hwndDlg, argv[1], false);
                }
                __except(0)
                {
                    MessageBoxA(hwndDlg, "Exception, please report to the author.", "Error", MB_ICONERROR);
                }
            }
        }
        LocalFree(argv);
    }
    return TRUE;

    case WM_DROPFILES:
    {
        wchar_t lpszFile[MAX_PATH] = L"";
        UINT uFile = 0;
        HDROP hDrop = (HDROP)wParam;
        uFile = DragQueryFileW(hDrop, 0xFFFFFFFF, NULL, 0);
        if (uFile != 1)
        {
            MessageBoxA(hwndDlg, "Dropping multiple files is not supported.", "Error", MB_ICONERROR);
            DragFinish(hDrop);
            break;
        }
        lpszFile[0] = '\0';
        if(DragQueryFileW(hDrop, 0, lpszFile, MAX_PATH))
        {
            if(GetFileAttributesW(lpszFile)==FILE_ATTRIBUTE_DIRECTORY)
                MessageBoxA(hwndDlg, "Directories are not supported.", "Error", MB_ICONERROR);
            else
            {
                __try
                {
                    LoadFile(hwndDlg, lpszFile, false);
                }
                __except(0)
                {
                    MessageBoxA(hwndDlg, "Exception, please report to the author.", "Error", MB_ICONERROR);
                }
            }
            DragFinish(hDrop);
        }
        return TRUE;
    }
    return FALSE;

    case WM_CLOSE:
    {
        EndDialog(hwndDlg, 0);
    }
    return TRUE;

    case WM_COMMAND:
    {
        switch(LOWORD(wParam))
        {
        case IDC_LIST_SECTIONS:
        {
            switch(HIWORD(wParam))
            {
            case LBN_SELCHANGE:
            {
                int selectedIndex=SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_GETCURSEL, 0, 0);
                char info[256]="";
                sprintf_s(info, " Relocation Directory RVA: %.8X\r\nRelocation Directory Size: %.8X", relocList.at(selectedIndex).VirtualAddress, relocList.at(selectedIndex).Size);
                SetDlgItemTextA(hwndDlg, IDC_EDT_INFO, info);
            }
            break;
            }
        }
        break;

        case IDC_BTN_FIX:
        {
            SetFocus(GetDlgItem(hwndDlg, IDC_EDT_INFO));
            int selectedIndex=SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_GETCURSEL, 0, 0);
            if(SetPE32DataW((wchar_t*)szSelectedFile, 0, UE_RELOCATIONTABLEADDRESS, relocList.at(selectedIndex).VirtualAddress) &&
                    SetPE32DataW((wchar_t*)szSelectedFile, 0, UE_RELOCATIONTABLESIZE, relocList.at(selectedIndex).Size))
            {
                SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_RESETCONTENT, 0, 0);
                for(unsigned int i=0; i<relocList.size(); i++)
                {
                    RelocInfo relocInfo=relocList.at(i);
                    char name[20]="";
                    if(relocInfo.VirtualAddress==relocList.at(selectedIndex).VirtualAddress)
                        sprintf_s(name, "->\"%s\"", relocInfo.name);
                    else
                        sprintf_s(name, "  \"%s\"", relocInfo.name);
                    SendDlgItemMessageA(hwndDlg, IDC_LIST_SECTIONS, LB_ADDSTRING, 0, (LPARAM)name);
                }
                MessageBoxA(hwndDlg, "New Relocation Directory set!", "Done", MB_ICONINFORMATION);
            }                
            else
                MessageBoxA(hwndDlg, "SetPE32Data failed...", "Error", MB_ICONERROR);
        }
        break;
        }
    }
    return TRUE;
    }
    return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    hInst=hInstance;
    InitCommonControls();
    return DialogBox(hInst, MAKEINTRESOURCE(DLG_MAIN), NULL, (DLGPROC)DlgMain);
}
